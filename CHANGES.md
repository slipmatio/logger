# Changelog

### 0.0.9 - 2020-11-07

- Fix: explicitly name files to include in tha npm package.

### 0.0.8 - 2020-11-07

- Fix: added manual scripts for better npm user experience.

### 0.0.7 - 2020-11-07

- Fix: added `exports` to package.json to allow native imports from dist folder.

### 0.0.6 - 2020-11-07

- Fix: refactor imports, fix build process.

### 0.0.5 - 2020-11-07

- Fix: refactor globals.

### 0.0.4 - 2020-11-07

- Fix: refactor globals.

### 0.0.3 - 2020-11-06

- Fix: ship proper TS declaration files.

### 0.0.2 - 2020-11-06

- Fix: ship all files.

### 0.0.1 - 2020-11-05

- First version.
